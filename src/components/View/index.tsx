import React, { FC } from 'react'
import classnames from 'classnames'
import styles from './view.module.scss'

export type IProps = {
  className?: string
  loading?: boolean
  inner?: boolean
}

const View: FC<IProps> = (props) => {
  const { className, children, loading = false, inner = false } = props
  const loadingStyle = {
    height: 'calc(100vh - 184px)',
    overflow: 'hidden',
  }

  return (
    <div
      className={classnames(className, {
        [styles.contentInner]: inner,
      })}
      style={loading ? loadingStyle : {}}
    >
      {children}
    </div>
  )
}

export default View
