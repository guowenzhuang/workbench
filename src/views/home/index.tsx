import { FC } from 'react'
import View from '@/components/View'
import { Row, Col } from 'antd'
import Task from './components/Task'
import Todo from './components/Todo'
import Post from './components/Post'
import './index.scss'

const Home: FC = () => {
  return (
    <View className="app-home">
      <Row gutter={16}>
        <Col className="gutter-row" span={16}>
          <div>
            <Task />
          </div>
          <div>
            <Todo />
          </div>
        </Col>
        <Col className="gutter-row" span={8}>
          <Post />
        </Col>
      </Row>
    </View>
  )
}

export default Home
