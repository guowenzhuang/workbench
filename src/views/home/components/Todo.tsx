import { FC } from 'react'
import { Table, Button } from 'antd'
import { ColumnsType } from 'antd/lib/table/interface'

const Todo: FC = () => {
  const columns:ColumnsType = [
    {
      title: '审核内容',
      dataIndex: 'taskName',
    },
    {
      title: '发起人',
      dataIndex: 'danger',
    },
    {
      title: '创建时间',
      dataIndex: 'jihua',
    },
    {
      title: '操作',
      dataIndex: '',
      key: 'x',
      render: () => <a href="./">去审批</a>,
    },
  ]
  const data = []
  return (
    <div className="todo">
      <div className="title">
        <h1>我的待办</h1>
        <Button type="primary">新建工单</Button>
      </div>
      <Table columns={columns} dataSource={data} />
    </div>
  )
}

export default Todo
