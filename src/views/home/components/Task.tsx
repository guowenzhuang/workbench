import { FC } from 'react'
import { Tabs, Table } from 'antd'
import { ColumnsType } from 'antd/lib/table/interface'

const { TabPane } = Tabs

const Task: FC = () => {
  const columns:ColumnsType = [
    {
      title: '任务名称',
      dataIndex: 'taskName',
    },
    {
      title: '当前阶段',
      dataIndex: 'danger',
    },
    {
      title: '计划发布时间',
      dataIndex: 'jihua',
    },
    {
      title: '创建人',
      dataIndex: 'create',
    },
    {
      title: '创建时间',
      dataIndex: 'createDate',
    },
    {
      title: '操作',
      dataIndex: '',
      key: 'x',
      render: () => <a href="/">查看详情</a>,
    },
  ]
  const data = []
  return (
    <div className="task">
      <Tabs defaultActiveKey="1" tabBarGutter={40}>
        <TabPane tab="我的发布" key="1">
          <Table columns={columns} dataSource={data} />
        </TabPane>
        <TabPane tab="我的环境" key="2">
          我的环境
        </TabPane>
        <TabPane tab="我的应用" key="3">
          我的应用
        </TabPane>
        <TabPane tab="我的第二方库" key="3">
          我的第二方库
        </TabPane>
      </Tabs>
    </div>
  )
}

export default Task
