import { FC } from 'react'
import { Redirect, Switch, withRouter, Route } from 'react-router-dom'
import setRouter from '@/routes'
import { Layout } from 'antd'
import { RouteComponentProps } from 'react-router'
import { IRoute } from '@/interface/router'
import { useStore } from '@/store'

const { Content } = Layout

const AppContent: FC<RouteComponentProps> = () => {
  const { appStore } = useStore()
  const { menus } = appStore
  const asyncRoutes: Array<IRoute> = setRouter(menus)
  const menuArr: IRoute[] = []
  const filterRouters = ((routes: Array<IRoute>) => {
    routes.forEach((route:IRoute) => {
      if (!route.children) {
        if (route.component) {
          menuArr.push(route)
        }
      } else {
        filterRouters(route.children)
      }
    })
  })
  filterRouters(asyncRoutes)
  return (
    <Content className="app-content">
      <Switch>
        <Redirect exact from="/" to="/home" />
        {menuArr.map((route: IRoute) => (
          <Route key={route.key} path={route.path} component={route.component} />
        ))}
        <Redirect to="/error/404" />
      </Switch>
    </Content>
  )
}

export default withRouter(AppContent)
