import { FC } from 'react'
import { RouteComponentProps } from 'react-router'
import { observer } from 'mobx-react'
import { Layout } from 'antd'
import { useStore } from '@/store'
import { MenuInfo } from '@/interface/router'
import AppHeader from './Header'
import AppContent from './Content'

const AppLayout: FC<RouteComponentProps> = () => {
  const { appStore } = useStore()

  const menus:Array<MenuInfo> = []
  const menu1 = {
    key: 'home',
    title: '首页',
    path: '/home',
  }
  menus.push(menu1)
  const subMenus2:Array<MenuInfo> = []
  const subMenu2 = {
    key: '2-1',
    title: '研发效率平台',
    path: './',
  }
  subMenus2.push(subMenu2)
  const subMenu3 = {
    key: '2-2',
    title: '应用管理',
    path: './',
  }
  subMenus2.push(subMenu3)
  const subMenu4 = {
    key: '2-4',
    title: '环境管理',
    path: './',
  }
  subMenus2.push(subMenu4)

  const menu2 = {
    key: '2',
    title: '工程效率',
    path: './',
    children: subMenus2,
  }
  menus.push(menu2)
  const menu3 = {
    key: '3',
    title: '运维',
    path: './',
  }
  menus.push(menu3)
  const menu4 = {
    key: '4',
    title: '中间件',
    path: './',
  }
  menus.push(menu4)
  const menu5 = {
    key: '5',
    title: '无线',
    path: './',
  }
  menus.push(menu5)
  const menu6 = {
    key: '6',
    title: '质量',
    path: './',
  }
  menus.push(menu6)
  const menu7 = {
    key: '7',
    title: '项目管理',
    path: './',
  }
  menus.push(menu7)
  const menu8 = {
    key: '8',
    title: '帮助',
    path: '/',
  }
  menus.push(menu8)
  appStore.setMenu(menus)
  return (
    <Layout style={{ height: '100vh', overflow: 'auto' }}>
      <AppHeader />
      <AppContent />
    </Layout>
  )
}
export default observer(AppLayout)
