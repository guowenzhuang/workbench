import React from 'react'

type ComponentType = React.ComponentType<any> & {name: string}

export interface IRouteBase {
  path: string
  title: string
  icon: string
  name: string
  key?: string
  exact?: boolean
  meta?: IRouteMeta
  component?: ComponentType
  children?: Array<IRouteBase>
  hidden?: Boolean
  // 302 跳转
  redirect?: string;
}

export interface IRouteMeta {
  tabFixed?: boolean
  isCache?: boolean
  hidden?: boolean
  name: string
  icon: string
}

export interface IRoute extends IRouteBase {
  children?: IRoute[];
}

export interface MenuInfo {
  key: string;
  title: string;
  path: string;
  iconClass?: string;
  component?: ComponentType;
  children?: Array<MenuInfo>;
}
