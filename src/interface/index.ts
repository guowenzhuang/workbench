import React from 'react'
import { AxiosRequestConfig } from 'axios'

export interface CustomAxiosRequestConfig extends AxiosRequestConfig {
  hideLoading?: boolean
}

export interface BaseResponse<T = any> {
  code: number
  data: T
  msg: string
}

type ComponentType = React.ComponentType<string> & {name: string}

export interface RouteItem{
  path: string
  title?: string
  icon?: string
  name: string
  key?: string
  exact?: boolean
  meta?: {
    tabFixed?: boolean
    isCache?: boolean
    hidden?: boolean
    name: string
    icon: string
  }
  component?: ComponentType
  children?: Array<RouteItem>
}
