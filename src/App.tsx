import React, { lazy } from 'react'
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import './App.css'
/* import { renderRoutes } from 'react-router-config'
import routes from '@/routes' */

const loading = () => <div className="loader" />
const AppLayout = lazy(() => import('@/layouts'))

function App() {
  return (
    <BrowserRouter>
      <React.Suspense fallback={loading()}>
        <Switch>
          {/* TODO layout */}
          {/* {renderRoutes(routes)} */}
          <Route
            path="/"
            render={(props) => {
              return <AppLayout {...props} />
            }}
          />
        </Switch>
      </React.Suspense>
    </BrowserRouter>
  )
}

export default App
