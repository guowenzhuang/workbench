interface TreeData {
  label: string
  value: string | number
  children?: Array<TreeData>
}

// 格式化antd treeSelect组件数据
export function getTreeSelectData(
  data: Array<any>,
  label?: string,
  value?: string,
  children?: string,
):Array<TreeData> {
  const filterData: Array<TreeData> = []
  data.forEach((item) => {
    const itemData: any = {}
    itemData.title = item[label || 'title']
    itemData.value = item[value || 'value']
    itemData.children = item[children || 'children'] ? getTreeSelectData(item.children, label, value, children) : []
    filterData.push(itemData)
  })
  return filterData
}
