import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'
import zhCN from 'antd/es/locale/zh_CN'
import dayjs from 'dayjs'
import { ConfigProvider } from 'antd'
import { Provider } from 'mobx-react'
import store from '@/store'
import App from './App'
import reportWebVitals from './reportWebVitals'

dayjs.locale('zh-cn')

ReactDOM.render(
  <ConfigProvider locale={zhCN}>
    <Provider {...store}>
      <App />
    </Provider>
  </ConfigProvider>,
  document.getElementById('root'),
)

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals()
