import { createContext, useContext } from 'react'
import appStore from './modules/app'

const store = {
  appStore,
}

const StoreContext = createContext(store)

export const useStore = () => useContext(StoreContext)

export default store
