import { action, computed, makeObservable, observable } from 'mobx'
// import { createContext } from 'react'
import { MenuInfo } from '@/interface/router'

class App {
  constructor() {
    makeObservable(this)
  }

  @observable menus: Array<MenuInfo> = []

  @observable currentMenuList: Array<MenuInfo> = []

  @observable theme = 'light'

  @observable collapsed = false

  @observable count = 0

  @action setMenu = (list: Array<MenuInfo>) => {
    this.menus = list
  }

  // 设置菜单显示状态
  @action setCurrentMenuList = (list: Array<MenuInfo>) => {
    this.currentMenuList = list
  }

  // 设置菜单显示状态
  @action setCollapsed = (type: boolean) => {
    this.collapsed = type
  }

  @action inc = (value: number) => {
    this.count += value
  }

  @computed get total() {
    console.log('entry')
    return this.count * 60
  }
}

export default new App()
// export default createContext<App>(new App())
