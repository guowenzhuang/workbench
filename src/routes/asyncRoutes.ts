import { lazy } from 'react'

export default {
  // 根据后端返回数据code字段映射组件
  home: lazy(() => import('@/views/home')),
}
