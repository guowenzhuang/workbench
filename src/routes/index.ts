import { IRoute, MenuInfo } from '@/interface/router'
import asyncRoutes from './asyncRoutes'

const router : Array<IRoute> = []

const formatRoutes = (menus: Array<MenuInfo>): Array<IRoute> => {
  const routes: IRoute[] = []
  if (!menus?.length) {
    return []
  }

  menus.forEach((menu) => {
    const { title, key, path, iconClass } = menu
    const route: IRoute = {
      title,
      path,
      name: key,
      icon: iconClass,
      key,
      component: asyncRoutes[key],
    }
    if (menu?.children?.length) {
      route.children = []
      route.children.push(...formatRoutes(menu.children))
    }
    routes.push(route)
  })
  return routes
}
const setRouter = (menus?:Array<MenuInfo>): Array<IRoute> => {
  let userRouter: IRoute[] = []
  if (menus?.length) {
    const asyncRouter = formatRoutes(menus)
    asyncRouter.push(...router)
    userRouter = asyncRouter
  } else {
    userRouter = router
  }
  return userRouter
}

export default setRouter
